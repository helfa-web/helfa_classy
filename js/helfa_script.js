console.log("Hello World!");

let h_main_nav_button = document.querySelector("#block-helfa-theme-module-main-navigation-button-block i");
let h_main_nav = document.querySelector("#block-helfa-classy-mainnavigation");

if (h_main_nav_button && h_main_nav) {
    console.log(h_main_nav_button.className);

    h_main_nav_button.addEventListener("click", ()=>{
        h_main_nav.classList.toggle("active");
    });
}

let h_search_box_button = document.querySelector("#block-helfa-theme-module-search-box-button-block i");
let h_search_box = document.querySelector("#block-helfa-classy-search-form");

if (h_search_box_button && h_search_box) {
    console.log(h_search_box_button.className);

    h_search_box_button.addEventListener("click", ()=>{
        h_search_box.classList.toggle("active");
    });
}

let h_account_button = document.querySelector("#block-helfa-theme-module-account-button-block i");
let h_account = document.querySelector("#block-helfa-classy-useraccountmenu-2");

if (h_account_button && h_account) {
    console.log(h_account_button.className);

    h_account_button.addEventListener("click", ()=>{
        h_account.classList.toggle("active");
    });
}

let h_admin_nav_button = document.querySelector("#block-helfa-theme-module-admin-navigation-button-block i");
let h_admin_nav = document.querySelector(".layout-sidebar-first");

if (h_admin_nav_button && h_admin_nav) {
    console.log(h_admin_nav_button.className);

    h_admin_nav_button.addEventListener("click", ()=>{
        h_admin_nav.classList.toggle("active");
    });
}


// -----Make menu sticky on the top-----
window.onscroll = function() {makeMenuSticky()};
window.onresize = function() {makeMenuSticky()};

let r_p_m = document.querySelector(".region-primary-menu");
let r_s_m = document.querySelector(".region-secondary-menu");
let layout_main = document.querySelector(".layout-main");

let sticky_menu_css_class = "sticky-menu";
let sticky_menu_2_col_css_class = "sticky-menu-2-col";
let sticky_menu_2_col_width = 800;
let width_100per = "width_100per";
let width_50per = "width_50per";

// Get the offset position of the navbar
let sticky = r_p_m.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function makeMenuSticky() {
    if (!r_p_m || !r_s_m) return;

    if (window.innerWidth < sticky_menu_2_col_width) {
        r_p_m.classList.add(width_50per);
        r_p_m.classList.remove(width_100per);
    } else {
        r_p_m.classList.add(width_100per);
        r_p_m.classList.remove(width_50per);
    }

    if (window.pageYOffset > sticky) {
        if (window.innerWidth < sticky_menu_2_col_width) {
            r_p_m.classList.add(sticky_menu_css_class);
            r_s_m.classList.add(sticky_menu_css_class);
            layout_main.classList.add(sticky_menu_css_class);
        } else {
            r_p_m.classList.add(sticky_menu_css_class);
            r_p_m.classList.add(sticky_menu_2_col_css_class);
            layout_main.classList.add(sticky_menu_css_class);
        }
    } else {
        r_p_m.classList.remove(sticky_menu_css_class);
        r_p_m.classList.remove(sticky_menu_2_col_css_class);
        r_s_m.classList.remove(sticky_menu_css_class);
        layout_main.classList.remove(sticky_menu_css_class);
    }
} 